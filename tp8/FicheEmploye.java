import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


public class FicheEmploye extends GridPane{
    private TestJDBC testJDBC;
    private Label titre;
    private TextField numEmp;
    private TextField nomEmp;
    private TextField prenomEmp;
    private TextField telephone;
    private TextField service;

    private Button bouton;

    public void setNomBouton(String nomBouton) {
        this.bouton.setText(nomBouton);

    }

     public void setTitre(String titre) {
        this.titre.setText(titre);
    }

    public void setNumEmp(int numEmp){
        this.numEmp.setText(""+numEmp);
    }

    public String getTitre(){
        return this.titre.getText();
    }


    public Employe getEmploye(){
        int id=-1;
        try {
            id = Integer.parseInt(this.numEmp.getText());
        }catch (Exception e){}
        String nomEmp=this.nomEmp.getText();
        String prenomEmp=this.prenomEmp.getText();
        String telephone=this.telephone.getText();
        int service=-1;
        try {
            service = Integer.parseInt(this.service.getText());
        }catch (Exception e){}
	    return new Employe(id,nomEmp,prenomEmp,telephone,service);
    }

    void viderFiche(){
        this.numEmp.setText("");
        this.nomEmp.setText("");
        this.prenomEmp.setText("");
        this.telephone.setText("");
        this.service.setText("");
    }



    FicheEmploye(TestJDBC testJDBC) {
        super();
        this.testJDBC=testJDBC;
        this.setPadding(new Insets(5, 5, 5, 5));
        this.setHgap(5);
        this.setVgap(5);
        this.setBackground(new Background(new BackgroundFill(Color.LIGHTSEAGREEN, null, null)));

        this.titre=new Label("Fiche Employe");
        this.titre.setFont(Font.font(24));
        this.numEmp = new TextField();
        this.nomEmp = new TextField();
        this.prenomEmp = new TextField();
        this.telephone = new TextField();
        this.service = new TextField();

        this.bouton = new Button("OK");
        this.bouton.setOnAction(new ControleurBouton(this.testJDBC));

        this.add(this.titre,1,0,2,1);
        this.add(new Label("Numéro:"),1,1);
        this.add(this.numEmp,2,1);
        this.add(new Label("Nom:"),1,2);
        this.add(this.nomEmp,2,2);
        this.add(new Label("Prénom:"),1,3);
        this.add(this.prenomEmp,2,3);
        this.add(new Label("Téléphone:"),1,4);
        this.add(this.telephone,2,4);
        this.add(new Label("Service:"),1,5);
        this.add(this.service,2,5);
        this.add(bouton,1,7,2,1);

    }
    public int getNumEmp(){
        return Integer.parseInt(this.numEmp.getText());
    }

}
