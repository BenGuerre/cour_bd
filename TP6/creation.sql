create database if not EXISTS Bibliotheque;
use Bibliotheque;
drop table ECRIRE;
drop table AUTEUR;
drop table LIVRE;
drop table GENRE;



create table AUTEUR (
    idaut varchar(50),
    nom varchar(50),
    prenom varchar(50),
    primary key (idaut)
);

create table ECRIRE (
    idaut varchar(50),
    idliv varchar(50),
    PRIMARY KEY (idaut, idliv)
);

create table LIVRE (
    idliv varchar(50),
    titre varchar(50),
    idgen VARCHAR(50),
    primary key (idliv)
);

create table GENRE (
    idgen varchar(50),
    nomgen varchar(50),
    primary key (idgen)
);

ALTER TABLE ECRIRE add FOREIGN KEY (idaut) REFERENCES AUTEUR;
ALTER Table ECRIRE add FOREIGN KEY (idliv) REFERENCES LIVRE;
ALTER TABLE LIVRE ADD FOREIGN KEY (idgen) REFERENCES GENRE;



-- REVOKE ALL PRIVILEGES ON Bibliotheque.* FROM adminbibli;
-- drop user if exists adminbibli;
REVOKE ALL PRIVILEGES ON Bibliotheque.* FROM biblio;
drop user if exists biblio;
REVOKE ALL PRIVILEGES ON Bibliotheque.* FROM visiteur;
drop user if exists visiteur;


-- create user if not exists adminbibli identified by '123';
-- grant all privileges on Bibliotheque.* to adminbibli with grant option;
create user if not exists biblio identified by 'biblio';
grant select, insert, delete, update on Bibliotheque.* to biblio;

create user if not exists visiteur identified by 'visiteur';
grant select on Bibliotheque.* to visiteur;
